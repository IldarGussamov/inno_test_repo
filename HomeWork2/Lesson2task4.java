import java.util.Scanner;
import java.util.Arrays;
public class Lesson2task4{

     public static void main(String []args){
         System.out.println("Enter array size: ");
         Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
		int temp;
		int positionOfMin = 0;
		int positionOfMax = 0;
		System.out.println("Enter array elements: ");
		int min = array[0];
        for (int i = 0; i < array.length; i++) {
			array [i] = scanner.nextInt();
            if (array[i] < min){
                min = array[i];
				positionOfMin = i;
            }
        }
		System.out.println(Arrays.toString(array));
		System.out.println("MIN element is " +min);
        int max = array[0];
		for (int i = 0; i < array.length; i++) {
				if (array[i] > max){
                max = array[i];
				positionOfMax = i;
			}
		}
        System.out.println("MAX element is " +max);
	temp = array[positionOfMin];
	array[positionOfMin] = array[positionOfMax];
	array[positionOfMax] = temp;
		System.out.print("Array after Swap ");
		System.out.print(Arrays.toString(array));
	 }
}