import java.util.Arrays;
import java.util.Scanner;
class Lesson3task1 {
	public static void arraySum (int array[]) {
		int arraySum = 0;
           for (int i = 0; i < array.length; i++){
           arraySum += array [i];
            }
		System.out.println("Array Sum: " + arraySum);
	}
	public static void ReverseArray (int array []) {
		for (int i = 0; i < array.length / 2; i++) {
		int swap = array[array.length - i - 1];
		array[array.length - i - 1] = array[i];
		array[i] = swap;
	}
	System.out.println ("Reversed Array: " + Arrays.toString(array));
	}
	public static void arrayAverage(int array []) {
		double sum = 0;
		double arrayAverage = 0;
		for(int i=0; i < array.length; i++){
        	sum += array[i];
			arrayAverage = sum / array.length;
		}
	System.out.println ("Array Average: " + arrayAverage);
	}
	public static void swapMinToMax (int array[]) {
		int min = array[0];
		int positionOfMin = 0;
		int max = array[0];
		int positionOfMax = 0;
		for (int i = 1; i < array.length; i++) {
			if (array[i] < min){
                min = array[i];
				positionOfMin = i;
			}
			if (array[i] > max){
                max = array[i];
				positionOfMax = i;
			}
		}
		int swap = array[positionOfMax];
		array[positionOfMax] = array[positionOfMin];
		array[positionOfMin] = swap;
		System.out.println("Array after Swap: " + Arrays.toString(array));	
	}
	public static void isArraySorted (int array []){
		boolean isArraySorted = false;
		int swap;
		while (!isArraySorted) {
			isArraySorted = true;
			for (int i = 0; i < array.length - 1; i++) {
				if(array[i] > array[i+1]){
					isArraySorted = false;
					
					swap = array[i];
                    array[i] = array[i+1];
                    array[i+1] = swap;
                }
            }
        }
        System.out.println("Buble Sorted Array: " + Arrays.toString(array));
    }
	public static void arrayToNumber(int array []){
		int number = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] < 10)
				number = number * 10 + (array[i]);
			if (array[i] >= 10)
				number = number * 100 + (array[i]);
			if (array[i] >= 100)
				number = number * 1000 + (array[i]);
			}
        System.out.println("Array Converted to number: " + number);
	}	
	public static void main(String []args){
	System.out.println ("Enter array size: ");
	
	Scanner scanner = new Scanner(System.in);
	int n = scanner.nextInt();
	int array[] = new int[n];
	
	System.out.println ("Enter array elements: ");
		for (int i = 0; i < array.length; i++) {
			array [i] = scanner.nextInt();
		}
		System.out.println("Primary Array: " + Arrays.toString(array));
		arraySum(array);
		ReverseArray(array);
		arrayAverage(array);
		swapMinToMax(array);
		isArraySorted(array);
		arrayToNumber(array);
		}
}