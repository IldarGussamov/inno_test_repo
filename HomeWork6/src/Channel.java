import java.util.Random;
public class Channel {
    private static Program program;
    private TV tv;
    public Channel(TV tv) {
        this.tv = tv;
    }
    //случайная программа при выборе канала
    public static String randomProg1() {
        Random random = new Random();
        int b = random.nextInt(7);
        return Program.ListOfProgram_1;
    }

    public static String randomProg2() {
        Random random = new Random();
        int b = random.nextInt(7);
        return Program.ListOfProgram_2;
    }

    public static String randomProg3() {
        Random random = new Random();
        int b = random.nextInt(7);
        return Program.ListOfProgram_3;
    }

    public static String randomProg4() {
        Random random = new Random();
        int b = random.nextInt(7);
        return Program.ListOfProgram_4;
    }

    public static setProgram(Program program) {
        Channel.program = program;
    }
}
