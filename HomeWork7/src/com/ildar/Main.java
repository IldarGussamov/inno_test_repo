package com.ildar;

public class Main {

    public static void main(String[] args) {
	User user1 = new User.Builder();
	.firstName("Firstname_1")
    .lastName("Lastname_1")
    .age(26)
    .isWorker(true)
    .build();

	User user2 = new User.Builder();
    .lastName("Lastname_2")
    .age(20)
    .isWorker(false)
    .build();

	user1.data();
	user2.data();
    }
}
