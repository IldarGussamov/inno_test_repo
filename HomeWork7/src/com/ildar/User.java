package com.ildar;

public class User {
    public String firstName;
    public String lastName;
    public int age;
    public boolean isWorker;

    public static class Builder {
        private String firstName = " ";
        private String lastName = " ";
        private int age = 0;
        private boolean isWorker = false;
    }

    public Builder() {
    }

    public Builder firstName(String text) {
        firstName = text;
        return this;
    }

    public Builder lastName(String text) {
        lastName = text;
        return this;
    }

    public Builder age(int value) {
        age = value;
        return this;
    }

    public Builder isWorker(boolean bool) {
        isWorker = bool;
        return this;
    }

    public User builder() {
        return new User(this);
    }

    private User(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.isWorker = builder.isWorker;
    }

    public void data() {
        if (firstName = firstName)
            System.out.println("Firstname is: " + firstName);
        if (lastName = lastName)
            System.out.println("Lastname is: " + lastName);
        if (age = age)
            System.out.println("Age: " + age);
        if (isWorker = true || isWorker = false)
            System.out.println("Is this user a Worker: " + isWorker);
    }
}


